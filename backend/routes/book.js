const express = require('express')
const db = require('../db')
const router = express.Router()

// 1. GET  --> Display Book using the name from Containerized MySQL

router.get('/find',(request,response) => {
    const {book_title} = request.query
    const statement = `select * from book where book_title = '${book_title}'`
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
        connection.end()
        response.send(result)
    })

})




// 2. POST --> ADD Book data into Containerized MySQL table

router.post('/add',(request, response) => {
    // book_id | book_title | publisher_name | author_name
    const { book_id, book_title, publisher_name, author_name} = request.body 
    const connection = db.openConnection()

    const statement = `INSERT INTO book 
    (book_id, book_title, publisher_name, author_name) 
    values
    ('${book_id}', '${book_title}', '${publisher_name}', '${author_name}')
    `

    connection.query(statement, (error, result) => {
        connection.end()
        response.send(result)
    })
})

// 3. UPDATE --> Update publisher_name and Author_name into Containerized MySQL table


router.put('/edit/:book_id',(request, response) => {
    // book_id | book_title | publisher_name | author_name
    const { publisher_name, author_name} = request.body 
    const {book_id} = request.params
    const connection = db.openConnection()

    const statement = `UPDATE book
    SET
    publisher_name = '${publisher_name}',
    author_name = '${author_name}'
    where book_id = '${book_id}'`

    connection.query(statement, (error, result) => {
        connection.end()
        response.send(result)
    })
})




// 4. DELETE --> Delete Book from Containerized MySQL

router.delete('/delete/:book_id',(request, response) => {
    // book_id | book_title | publisher_name | author_name
    const {book_id} = request.params
    const connection = db.openConnection()

    const statement = `DELETE FROM book
    where book_id = '${book_id}'`

    connection.query(statement, (error, result) => {
        connection.end()
        response.send(result)
    })
})

module.exports = router