const express = require('express')
const app = express()

const routerBook = require('./routes/book')

app.use(express.json())

app.use(routerBook)

app.listen(4000, () => {
    console.log("Server started on port 4000")
})